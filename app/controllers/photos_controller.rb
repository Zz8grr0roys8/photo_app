class PhotosController < ApplicationController
    before_action :authenticate_user!, only: [:edit, :update, :destroy]
    
    def index
       @photos = Photo.all
    end
    
    def show
       @photo = Photo.find(params[:id])
    end
    
    def new
       @photo = current_user.photos.build
    end
    
    def edit
       @photo = Photo.find(params[:id])
    end
    
    def create
        @photo = current_user.photos.build(photo_params)
       
       if @photo.save
           redirect_to @photo
           flash[:success] = "写真の作成に成功しました"
       else
           render 'new'
       end
    end
    
    def update
        @photo = Photo.find(params[:id])
        
        if @photo.update(photo_params
        )
        redirect_to @photo
        else
            render 'edit'
        end
    end
    
    def destroy
       @photo = Photo.find(params[:id])
       @photo.destroy
       flash[:success] = "写真の削除に成功しました"
       redirect_to photos_path
    end
    
    private 
       def photo_params
           params.require(:photo).permit(:title, :caption, :avatar)
       end
    
    
end
