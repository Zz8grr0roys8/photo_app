class Photo < ApplicationRecord
    has_attached_file :avatar, styles: { medium: "300x300>", thumb: "100x100>" }, default_url: "/images/:style/missing.png"
  validates_attachment_content_type :avatar, content_type: /\Aimage\/.*\z/

   validates :title, presence: true, length: { maximum: 50}, uniqueness: true
   validates :caption, presence: true, length: { maximum: 300}, uniqueness: true
   validates :avatar, presence: true
   
   belongs_to :user
end
