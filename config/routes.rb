Rails.application.routes.draw do
  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'photos#index'
  get 'pages/about'
  get 'pages/contact'
  get 'pages/faq'
  resources :photos
  devise_for :users
end
