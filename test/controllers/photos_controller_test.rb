require 'test_helper'

class PhotosControllerTest < ActionDispatch::IntegrationTest
  # test "the truth" do
  #   assert true
  # end
  test "should get edit" do
    get edit_photo_path
    assert_response :success
  end
  
  test "should get index" do
    get photos_path
    assert_response :success
  end
  
  test "should get new" do
    get new_photo_path
    assert_response :success
  end
  
  test "should get show" do
    get photo_path
    assert_response :success
  end
  
  
end
